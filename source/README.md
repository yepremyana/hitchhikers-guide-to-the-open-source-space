## Hitchhiker Utilities Source Code

The scripts in this directory include the code for the Command Line Interface (CLI) and 
the implementation of the following features:
- **space_age.py** : Calculation of age in other worlds (including planets and other celestial bodies)<br /> 
Then inputs are month, date, year of birth and world.

- **space_weight.py** : Calculation of weight in other worlds (including planets and other celestial bodies) <br />
The inputs are weight in kgs and destination.

- **travel_time.py** : Calculation of space travel time. <br />
The inputs are the destination and the speed of travel.

- **main.py** : Implementation of the command line interface.