import argparse
from datetime import datetime
import json
import re
from . import space_age
from . import space_weight
from . import travel_time


"""
This is the command line interface to the hitchhiker tools.
"""


def planet(p):
    """
    The function will validate acceptable planets.
    :return:
    """
    planets = ['sun',
               'moon',
               'mercury',
               'venus',
               'earth',
               'mars',
               'jupiter',
               'saturn',
               'uranus',
               'neptune'
               ]
    if p.lower() in planets:
        return p
    else:
        raise argparse.ArgumentError("{} is not supported. Please try again with a valid input. Valid inputs are: {}"
                                     .format(p, json.dumps(planets)))


def weight(w):
    """
    This function validates the weight provided.
    It should be in the format: dd lbs or dd kgs
    This function should extract the value from the string inputed.
    :param w:
    :return:
    """
    match_pattern = "(?P<weight>\d{2,3}?)(?P<unit>[a-z]*?)$"
    try:
        m = re.match(match_pattern, w)
        if len(m.group("weight")) != 0 or len(m.group("unit")) != 0:
            return float(m.group("weight")), m.group("unit")
        else:
            raise Exception
    except Exception:
        raise


def month(m):
    """
    This function should check if value is a valid month
    :param m:
    :return:
    """
    if type(m) is str:
        m = int(m)
    if type(m) is not int:
        raise ValueError("Month must be an integer ranging between 1 and 12")
    if m < 1 or m > 12:
        raise ValueError("Month must be an integer ranging between 1 and 12")
    return m


def date(d):
    """
    This function should check if value is a valid month
    :param m:
    :return:
    """
    if type(d) is str:
        d = int(d)
    if type(d) is not int:
        raise ValueError("Month must be an integer ranging between 1 and 31")
    if d < 1 or d > 31:
        raise ValueError("Month must be an integer ranging between 1 and 31")
    return d


def year(y):
    """
    This function checks the value of the year.
    :param y:
    :return:
    """
    if type(y) is str:
        y = int(y)
    if y < 1900 or y > datetime.now().year:
        raise ValueError("Year must be an integer greater than 1900 be less than the current year.")
    return y


# def convert_to_kgs(weight_in_lbs):
#     """
#     This function converts weight from lbs to kgs
#     :param weight_in_lbs:
#     :return:
#     """
#     return weight_in_lbs*0.45359237


def main():
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(dest='subparser_name', help='help for subcommand')

    # create the parser for the "age" command
    parser_age = subparsers.add_parser("age", help="Find how old you are in another world.")
    parser_age.add_argument("-m", "--month", required=True, type=month, help="month of birth as int")
    parser_age.add_argument("-d", "--date", required=True, type=date, help="date of birth as int")
    parser_age.add_argument("-y", "--year", required=True, type=year, help="year of birth as int")
    parser_age.add_argument("-W", "--world", required=True, type=planet, help="planet or other celestial body of choice")

    # create the parser for the "weight" command
    parser_weight = subparsers.add_parser('weight', help='Find your weight on another planet.')
    parser_weight.add_argument("-w", "--weight",  required=True, type=weight,
                               help='Enter your weight with unit, e.g. 45kgs')
    parser_weight.add_argument('-W', "--world", required=True, type=planet,
                               help='planet or other celestial body of choice')

    # create the parser for the "travel" command
    parser_travel = subparsers.add_parser('travel', help='Find out how long space travel takes!')
    args = parser.parse_args()

    # parse the command and execute the desired feature
    if "age" == args.subparser_name:
        print("Calculating your age in another world ...")
        print(space_age.HitchHikerAge(args.date, args.month, args.year, args.world).years_on_planet())
    elif "weight" == args.subparser_name:
        print("Calculating your weight in another world ...")
        w, unit = args.weight
        # if unit == "lbs" or unit == "lb":
        #     w = convert_to_kgs(w)
        print(space_weight.HitchHikerWeight(w, args.world).space_weight())
    elif "travel" == args.subparser_name:
        print("Display options")


if __name__ == '__main__':
    main()
