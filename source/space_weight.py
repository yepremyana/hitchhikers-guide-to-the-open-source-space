
class HitchHikerWeight(object):
    """
    Calculates the hitch hiker's weight on a selected destination.

    Speed - defined by constant speed of time travel.
    Planet - defined by time taken for a full revolution around the sun.

    Attributes
    ----------
    destination: str
        A user provided input of planet they would like to get their weight on
    earth_weight: float
        Their weight on Earth in kgs
    """

    def __init__(self, earth_weight: float, destination: str) -> None:

        self.earth_weight = earth_weight
        self.destination = destination

        """
        The gravity of every destination is expressed in m/s^2
        """
        self.planet_gravity = {
            'sun': 274,
            'moon': 1.62,
            'mercury': 3.7,
            'venus': 8.87,
            'earth': 9.807,
            'mars': 3.711,
            'jupiter': 24.79,
            'saturn': 10.44,
            'uranus': 8.87,
            'neptune': 11.15
        }

    def space_weight(self) -> float:
        """
        Returns weight on a destination in kgs.

        Returns
        -------
        Float
            Weight on destination
        """

        return (self.earth_weight/self.planet_gravity["earth"])*self.planet_gravity[self.destination]
