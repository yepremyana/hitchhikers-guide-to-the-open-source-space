import datetime

class HitchHikerAge(object):
    """
    Calculates age on other planets based on birthdate of user. 

    Day - defined by time taken for a full rotation on the axis of the planet.
    Year - defined by time taken for a full revolution around the sun.

    Attributes
    ----------
    birth_day : int
        A user provided input for their birth date.
    birth_month : int
        A user provided input for their birth month.
    birth_year : int
        A user provided input for their birth year.
    planet : str
        A user provided input for the planet they wish to calculate their age on
    """

    def __init__(self, birth_day: int, birth_month: int, birth_year: int, planet: str) -> None:
        self.birth_day = birth_day
        self.birth_month = birth_month
        self.birth_year = birth_year
        self.planet = planet
        self.now = datetime.datetime.now()

        self.planet_meta = {
            'mercury': {
                'revolution': 87.97,
                'rotation': 58.6,
                'unit': 'mercurian'
            },
            'venus': {
                'revolution': 227.7,
                'rotation': 243,
                'unit': 'venusian',
            },
            'mars': {
                'revolution': 686.2,
                'rotation': 1.03,
                'unit': 'martian'
            },
            'jupiter': {
                'revolution': 4328.9,
                'rotation': 0.41,
                'unit': 'jovian'
            },
            'saturn': {
                'revolution': 10818.6,
                'rotation': 0.45,
                'unit': 'saturian'
            },
            'uranus': {
                'revolution': 30663.45,
                'rotation': 0.72,
                'unit': 'uranian'
            },
            'neptune': {
                'revolution': 60148.35,
                'rotation': 0.67,
                'unit': 'neptunian'
            }
        }

    def days_on_earth(self) -> float:
        """
        Returns age in number of earth days.

        Returns
        -------
        Float
            Number of earth days old.
        """

        return (self.now.year - self.birth_year) * 365.26 + (self.now.month - self.birth_month) * 30 + (self.now.day - self.birth_day) * 1

    def days_on_planet(self) -> float:
        """
        Returns age in number of planet days.

        Returns
        -------
        Float
            Number of planet days old.
        """

        return self.days_on_earth()/self.planet_meta[self.planet]['rotation']

    def years_on_planet(self) -> float:
        """
        Returns age in number of planet years.

        Returns
        -------
        Float
            Number of planet years old.
        """

        return self.days_on_earth()/self.planet_meta[self.planet]['revolution']
