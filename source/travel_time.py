
class HitchHikerTravelTime(object):
    """
    Calculates the time it'll take to travel from Earth to a selected destination
    and a selected speed.

    Speed - defined by constant speed of time travel.
    Planet - defined by time taken for a full revolution around the sun.

    Attributes
    ----------
    destination: str
        A user provided desired destination of space travel
    speed: float
        A user provided desired speed of space travel
    """

    def __init__(self, speed: float, destination: str) -> None:

        self.speed = speed
        self.destination = destination

        """
        The distances between the Earth and the objects within the solar system
        assume the shortest distances possible between the objects when 
        their orbits are aligned on the same side of the sun.
        All distance are in kms.
        """
        self.distance = {
            'sun': 149597870.691,
            'moon': 384400.0,
            'mercury': 91697870.691,
            'venus': 41597870.691,
            'mars': 78402129.309,
            'jupiter': 628402129.309,
            'saturn': 1280402129.309,
            'uranus': 2720402129.309,
            'neptune': 4350402129.309
        }

    def travel_time(self) -> float:
        """
        Returns time to travel to destination in seconds.

        Returns
        -------
        Float
            Seconds to travel to destination
        """
        return self.distance[self.destination]/self.speed
