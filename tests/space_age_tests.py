from source.space_age import *
import datetime
import unittest

class SpaceAgeTestSuite(unittest.TestCase):

    # Runs before every test case.
    def setUp(self, planet = 'mercury'):
        self.age = HitchHikerAge(12, 10, 1995, planet)
        self.age.now = datetime.datetime.strptime('2019-10-02', '%Y-%m-%d')

    def test_days_on_earth(self):
        self.assertEqual(self.age.days_on_earth(), 8756.24)

    def test_days_on_planet(self):
        self.assertEqual(self.age.days_on_planet(), 149.42389078498292)

    # When Issue ### gets fixed, please comment the following line.
    @unittest.skip('Skipping since issue #### is still open.')
    def test_days_on_planet_pluto(self):
        self.setUp('pluto')
        self.assertAlmostEqual(self.age.days_on_planet(), 1370.303599374022, places=0)

    def test_years_on_planet(self):
        self.assertEqual(self.age.years_on_planet(), 99.53666022507673)

    # When Issue ### gets fixed, please comment the following line.
    @unittest.skip('Skipping since issue #### is still open.')
    def test_years_on_planet_pluto(self):
        self.setUp('pluto')
        self.assertAlmostEqual(self.age.years_on_planet(), 35.318812520167796, places=0)

if __name__ == '__main__':
    unittest.main()
