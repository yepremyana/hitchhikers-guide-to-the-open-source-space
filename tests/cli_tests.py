import unittest
import subprocess

class CLITestSuite(unittest.TestCase):

    @unittest.skip("Issue 12 Test Case A")
    def test_weight_kgs(self):
        result = subprocess.check_output(['hitchhiker', 'weight', '-w', '50kgs', '-W', 'sun'])
        self.assertEqual(result, 'Calculating your weight in another world ...\n1396.9613541348017\n'.encode('UTF-8'))

    @unittest.skip("Issue 12 Test Case B")
    def test_weight_lbs(self):
        result = subprocess.check_output(['hitchhiker', 'weight', '-w', '110lbs', '-W', 'sun'])
        self.assertEqual(result, 'Calculating your weight in another world ...\n1394.0322251249108\n'.encode('UTF-8'))

if __name__ == '__main__':
    unittest.main()