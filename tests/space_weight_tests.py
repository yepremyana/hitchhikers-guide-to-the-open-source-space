from source.space_weight import *
import unittest

class SpaceWeightTestSuite(unittest.TestCase):

    def test_space_weight(self):
        self.assertEqual(HitchHikerWeight(1.0, 'sun').space_weight(), 27.939227082696032)

if __name__ == '__main__':
    unittest.main()