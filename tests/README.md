## Hitchhiker Utilities Test Cases

The scripts in this directory are the test cases run in the CI/CD pipeline:
- **space_age.py** : Calculation of age in other worlds (including planets and other celestial bodies)<br /> 
Then inputs are month, date, year of birth and world.

- **space_age_tests.py** : This tests the functionality of the space_age functions.

- **space_weight_tests.py** : This tests the functionality of space_weight functions.

- **cli_tests.py** : Tests the command line interface usage.