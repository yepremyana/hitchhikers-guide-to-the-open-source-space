from source.travel_time import *
import unittest

class TravelTimeTestSuite(unittest.TestCase):

    def test_travel_time(self):
        self.assertEqual(HitchHikerTravelTime(299700, 'sun').travel_time(), 499.15872769769777)

if __name__ == '__main__':
    unittest.main()