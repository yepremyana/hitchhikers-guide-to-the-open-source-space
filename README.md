# The Hitchhikers Guide to the Open Source Space

This is a sample repo for the Grace Hopper Celebration'19 Workshop called 'Hitchhikers Guide to the Open Source Space'.
We often use Open Source projects, but working with and contributing back to one for the first time can be an intimidating process. We want our participants to learn how to take their first step. Our goal is to familiarize participants with the concept of an open source project, the process of identifying improvements or bugs in the project and how to contribute to it.

## Table of contents

* [General info](#general-info)
* [Installation](#installation)
* [Dependencies](#dependencies)
* [Usage](#usage)
* [Authors](#authors)
* [Acknowledgements](#acknowledgements)

## General info
We have created some utilities for all our future hitchhikers. This project currently offers features to:
- Find out your age in another world.
- Find your weight in another world.
- Find out how long space travel takes.

The way you interact with this tool is using the command line. And this called a command line interface or CLI. 
Below are instructions to install and use the CLI.

To see more information regarding the code base and the tests. Please look at the following READMEs:
- https://gitlab.com/yepremyana/hitchhikers-guide-to-the-open-source-space/blob/master/source/README.md
- https://gitlab.com/yepremyana/hitchhikers-guide-to-the-open-source-space/blob/master/source/README.md

## Installation
1. Clone the git repository
> git clone https://gitlab.com/yepremyana/hitchhikers-guide-to-the-open-source-space.git
2. Go into the project directory
> cd hitchhikers-guide-to-the-open-source-space
3. Install the hitchhiker toolkit
> sudo pip3 install -e .

## Dependencies

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install the following dependencies.

## Usage
You can use `hitchhiker` on the command line to use all it's functionalities.
```bash
$ hitchhiker -h
usage: hitchhiker [-h] {age,weight,travel} ...

positional arguments:
  {age,weight,travel}  help for subcommand
    age                Find how old you are in another world.
    weight             Find your weight on another planet.
    travel             Find out how long space travel takes!

optional arguments:
  -h, --help           show this help message and exit
```
To get further usage information for a specific subcommand, you can do:
```bash
$ hitchhiker age -h
usage: hitchhiker age [-h] [-m MONTH] [-d DATE] [-y YEAR] [-p PLANET]

optional arguments:
  -h, --help            show this help message and exit
  -m MONTH, --month MONTH
                        month of birth as int
  -d DATE, --date DATE  date of birth as int
  -y YEAR, --year YEAR  year of birth as int
  -p PLANET, --planet PLANET
                        planet of choice
                        
$ hitchhiker age -m 9 -d 22 -y 1995 -W uranus
Calculating your age in another world ...
0.28595086332425085
```
For usage of space weight:
```bash
$ hitchhiker weight -h
usage: hitchhiker weight [-h] -w WEIGHT -W WORLD

optional arguments:
  -h, --help            show this help message and exit
  -w WEIGHT, --weight WEIGHT
                        Enter your weight with unit, e.g. 45kgs
  -W WORLD, --world WORLD
                        planet or other celestial body of choice

$ hitchhiker weight -w 50kgs -W sun
Calculating your weight in another world ...
1396.9613541348017

```
For usage of space travel:
```
hitchhiker travel -h
usage: hitchhiker travel [-h]

optional arguments:
  -h, --help  show this help message and exit
```

## Authors

- [Roxana González Burgos]()
- [Namrata Malarout]()
- [Alice Yepremyan]()

## Acknowledgements
For Space Age:
https://projects.raspberrypi.org/en/projects/space-age/3
https://museumsvictoria.com.au/media/1869/calculate-your-age-on-other-planets.pdf
https://www.exploratorium.edu/ronh/age/

For Space Weight
http://www.spacecenter.org/docs/Activities-HowMuchDoIWeigh.pdf
https://theplanets.org/weight-on-planets/
https://www.exploratorium.edu/ronh/weight/

For Space Travel:
http://nathangeffen.webfactional.com/spacetravel/spacetravel.php
https://www.vcalc.com/wiki/vCalc/Astro+Travel+Time 


