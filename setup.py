from setuptools import setup
setup(
    name='hitchhiker',
    version='0.0.1',
    packages=['source'],
    entry_points = {
        'console_scripts': [
            'hitchhiker = source.main:main'
        ]
    },
    install_requires=[]
)
