## How To Contribute (We need to elaborate more here)

Participants at this workshop are invited to Contribute to this repository.
To do so:
1. Go to [Google Spreadsheets](https://docs.google.com/spreadsheets/d/1iJNIt9k7oNVVnIc4ZFw2JBy1tE_eti49cvW5lZiH1B4/edit?usp=sharing). At the document, select an account of your preference, and add your name in the `Name of Participant` column. If you already have a GitLab account, you can also use your credentials.
2. Read the [Usage](#usage) of the tool to familiarize with the functionality.
3. See the [issues listed GitLab](https://gitlab.com/yepremyana/hitchhikers-guide-to-the-open-source-space/-/boards) and choose an issue of your preference. Please note, that these are tagged as [Beginner](https://gitlab.com/yepremyana/hitchhikers-guide-to-the-open-source-space/issues?label_name%5B%5D=Beginner), [Intermediate](https://gitlab.com/yepremyana/hitchhikers-guide-to-the-open-source-space/issues?label_name%5B%5D=Intermediate) and [Advanced](https://gitlab.com/yepremyana/hitchhikers-guide-to-the-open-source-space/issues?label_name%5B%5D=Advanced), choose your level of preference.
4. In the [Board](https://gitlab.com/yepremyana/hitchhikers-guide-to-the-open-source-space/-/boards), assign the ticket to your username, and move it to `In Progress`. You can add comments and start a conversation in case the Issues does not provide enough information.
5. The source code is currently available at [Repl.it](https://repl.it/@rogonzalez/HitchickerGuideToOpenSource?language=python3&folderId=), in where you can edit freely.
6. Create a branch with the issue name 
    
    ```sh
    # branch naming conventions:
    # Name as [type of issue]-[username]-[issue number]
    # use '-'s to separate words
    
    # examples:
    # feature-hitchhiker000-12, docs-hitchhiker000-12 , bug-hitchhiker000-12
    $ git -b checkout [branch-name]
    ```

7. Add and run test cases that cover the changes made. To do so, you need to run:

    ```sh
    # runs space_age test cases
    python tests/space_age_tests.py

    # runs space_weight test cases
    python tests/space_weight_tests.py

    # runs travel_time test cases
    python tests/travel_time_tests.py
    ```

8. Push the code to GitLab

    ```sh
    # if pushing to a new branch for the first time
    $ git push --set-upstream origin [branch-name]
    
    # otherwise
    $ git push
    ```

9. Create a Pull Request